# RCHE認證考試與學習路徑

## RCHE8 認證考試與學習路徑
```mermaid
graph TB;
  124(RH124)-->134(RH134)-->EX200(Red hat Certified System Administrator Exam `EX200` )-->RHCSA(RH294 自動化)-->RHCE(Red hat Certified Enginner `RHCE` exam for Red Hat Enterprise Linux 8)-->RHCEA(Red Hat Certified Engineer RHCE認證);
 
```

**考試通過標準：滿分為300分，通過標準為210分**