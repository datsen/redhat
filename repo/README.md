# 如何使用Repostority 手冊
## 1. 檢驗所取得的CentOS Linux映像、ISO或套件
   >.asc檔(eq.sha256sum.txt.asc)可用來檢驗該ISO檔
## CentOS系列

1. 檢驗簽署檔案用的金鑰
首先用以下指令（在指令行）建立一個檢驗用的目錄：


    mkdir validate
    
    cd validate

接著便是檢驗簽署 sha256sum 檔的金鑰。你可以從[這裡](https://www.centos.org/keys/)找到金鑰及相應指模的清單。在這個範例中我們會檢驗 CentOS-7 的金鑰……它的名稱是 RPM-GPG-KEY-CentOS-7。

> [CentOS keys](http://mirror.centos.org/centos/) 

這邊資訊待整理

----

### 使用ISO（或映像）檔及 sha256sum.txt.asc 檔進行檢驗

1.  weget http://ftp.stu.edu.tw/Linux/CentOS/8.5.2111/isos/x86_64/CentOS-8.5.2111-x86_64-boot.iso

2. 取得 ISO 檔後，同一處有.asc 檔。
3. 基本的步驟是

    1. 檢驗金鑰
    2. 匯入金鑰
    3. 取得映像或 ISO 檔
    4. 從 ISO 或映像檔的下載目錄取得 .asc 檔
    5. 檢驗 .asc 的簽名
    6. 利用 .asc 檢驗 ISO 或映像檔

通過上述步驟，你可隨時檢驗所擁有的 ISO 或映像檔是由 CentOS Linux 小組發行的。你從那裡下載金鑰或映像／ISO 檔並不要緊……因為這個程序依賴 gpg 金鑰的簽名。

---


## **利用已簽署的 CentOS-6 或 CentOS-7 中繼資料存取 CentOS 計劃發行的軟件庫**

> 已簽署的軟件庫中繼資料是甚麼
運用已簽署的中繼資料等於 yum 檢查 repomd.xml.asc 是經由軟件庫的金鑰所簽署的。這代表你能確定資訊是由金鑰的擁有人所發行的。

## **如何啟用己簽署的中繼資料**
請編輯相關的軟件庫定義檔，然後在適當軟件庫部份加入以下內容：
位置:  **/etc/yum.repos.d/CentOS-Base.repo**
repo_gpgcheck=1

---

# ISO用法

Step 1: Download/Transfer CentOS DVD ISO
 
 wget  http://ftp.stu.edu.tw/Linux/CentOS/8.5.2111/isos/x86_64/CentOS-8.5.2111-x86_64-boot.iso

 Step 2: Mount CentOS DVD ISO

 To view the CentOS DVD ISO data, we first need to mount it on desired location. We usually mount CD-ROM, USB devices or ISO files to /mnt directory (if free to use). To mount CentOS DVD ISO run the following command from console (please change /path/to/iso and /mnt accordingly):
    
    [root@demo]# mount -o loop /path/to/iso /mnt

Step 3: Create YUM Repository Configuration file

To start using the newly created Custom YUM Repository we must create YUM Repository Configuration file with .repo extension, which must be placed to /etc/yum.repos.d/ directory. Instructions to create YUM Repository Configuration file are covered in the first topic of this article called "YUM Repository Configuration File".

### **如何寫自己的repository**

Example CentOS DVD ISO YUM Repository Configuration file:
/etc/yum.repos.d/centosdvdiso.repo

    [centosdvdiso]
    name=CentOS DVD ISO
    baseurl=file:///mnt
    enabled=1
    gpgcheck=1
    gpgkey=file:///mnt/RPM-GPG-KEY-CentOS-6

如何客製化repository

Custom YUM Repository
Sometimes we need to create a Custom YUM Repository (handy when the VPS has no internet connection). We can create a Custom YUM Repository from a desired number of selected RPM package files. Custom YUM Repository only holds the RPM package files we want to include in.

Step 1: Install "createrepo"
To create Custom YUM Repository we need to install additional software called "createrepo" on our cloud server. We can install "createrepo" by running the following command from console:

yum install createrepo
Step 2: Create Repository directory
We need to create a new directory that will be the location of our Custom YUM Repository and will hold the desired RPM package files. We can do this with the following command from console (choose a different /repository1 directory name if you like):

mkdir /repository1
Step 3: Put RPM files to Repository directory
If RPM package files are not yet present on our VPS we need to transfer them to our cloud server via FTP or SSH - use software like WinSCP (free SFTP client and FTP) or similar. We can also download RPM package files directly to our VPS (internet connection needed) with "wget" command from console (please change HTTP link accordingly):

wget http://mirror.lihnidos.org/CentOS/6/os/i386/Packages/NetworkManager-0.8.1-43.el6.i686.rpm

If RPM files are already present on our VPS, we need to Copy or Move these files to the newly created directory from "Step 2". We can move RPM files with the following command from console (please change /path/to/rpm and /repository1 accordingly):

mv /path/to/rpm /repository1
We can copy RPM files with the following command from console (please change /path/to/rpm and /repository1 accordingly):

cp /path/to/rpm /repository1
Step 4: Run "createrepo"
Createrepo command reads through Custom YUM Repository directory from "Step 2" and creates a new directory called "repodata" in it. Repodata directory holds the metadata information for the newly created repository. Every time we add additional RPM package files to our Custom YUM Repository, we need to re-create Repository metadata with "createrepo" command. We can create new repository metadata by running the following command from console (please change /repository1 accordingly):

createrepo /repository1
Step 5: Create YUM Repository Configuration file
To start using the newly created Custom YUM Repository, we must create the corresponding YUM Repository Configuration file with .repo extension, which must be placed to /etc/yum.repos.d/ directory. Instructions to create YUM Repository Configuration file are covered in the first topic of this article called "YUM Repository Configuration File".

Example Custom YUM Repository Configuration file:
/etc/yum.repos.d/custom.repo

    [customrepo]
    name=Custom Repository
    baseurl=file:///repository1/
    enabled=1
    gpgcheck=0

# 掛載之後還是無法使用yum??

yum 指令
    
    yum makecache     
    yum check-update
    yum update
    yum update kernel
    yum upgrade
    yum clean packages

可以使用的路徑
http://mirror.centos.org/centos/8/AppStream/x86_64/os/

只要找到正確的來源，以及正確的gpgkey寫到.repo裡就可以修改repository來源

> example.repo

    [base]
    name=CentOS-$releasever - BASE
    baseurl=http://mirror.centos.org/centos/8/AppStream/x86_64/os/
    gpgcheck=1
    gpgkey=http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-Official

![Alt](/repo/media/log.png)
![Alt](/repo/media/gpgkeys.png)