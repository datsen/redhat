# 安裝完之後會出現的問題


    [root@localhost ~]# yum update
    Failed to set locale, defaulting to C.UTF-8
    Updating Subscription Management repositories.
    Unable to read consumer identity

    This system is not registered to Red Hat Subscription Management. You can use subscription-manager to register.
第一個會出現的，就是此系統沒有註冊過Red Hat。提示使用``subscription-managenment register ``這個指令去[註冊](https://www.redhat.com/wapps/ugc/register.html)

    Error: There are no enabled repositories in "/etc/yum.repos.d", "/etc/yum/repos.d", "/etc/distro.repos.d".
第二個錯誤訊息是沒有可以用的 ``repositories``

----
### 如何解決

1. 先使用它的提示命令去做註冊的動作

    >[root@localhost ~]# subscription-manager register --username=YOURUSERNAME --password=YOURPASSWORD

接下來就會看到這組訊息

    You are attempting to use a locale: "zh_TW.UTF-8" that is not fully supported by this system.
    正在向 subscription.rhsm.redhat.com:443/subscription 註冊
    HTTP error (401 - Unauthorized): 無效的使用者名稱或密碼。若要建立一組帳號，請至 https://www.redhat.com/wapps/ugc/register.html

### 將repo來源更改為本地下載的CentOS版本，至於要用哪一種版本就看
1. 這邊使用[CentOS8](http://mirror.cisp.com/CentOS/8/isos/x86_64/)
> **YUM Repostory Configuation file**
> ``YUM`` 是 **Red Hat** 和 **CentOS** 安裝新軟體的控制台。
> 第一次使用 command line 必須確認 `` **YUM Repository** ``是不是存在這個路徑 ``/etc/yum.repos.d/ directory``。

> YUM Repository的配置
> --------
> 1. 路徑: /etc/yum.repos.d/
> 2. 副檔名必須為 **`.repo`**，才能被 **`YUM`** 識別

>  YUM Repository 配置(有顏色的是必填)
> 1. **`Repository ID`**
> 2. **`Name`**
> 3. **`Baseurl`**
> 4. <font face="黑體" size=3>**`Enabled`**</font>
> 5. Gpgcheck
> 6. Gpgkey
> 7. Exclude
> 8. Includepkgs

### Step 1 建立YUM Repository 配置檔 [如何使用yum](/yum/README.md)
建立.repo副檔名的檔案，並放置在`/etc/yum.repos.d/`
> [examplerepo]
> 
> name=Example Repositorybaseurl=http://mirror.cisp.com/CentOS/6/os/i386/
> 
> enabled=1
> 
> gpgcheck=1
> 
> gpgkey=http://mirror.cisp.com/CentOS/6/os/i386/RPM-GPG-KEY-CentOS-6

[**`.repo建立流程`**](/repo/README.md)

[參考資料](https://www.digitalocean.com/community/tutorials/how-to-set-up-and-use-yum-repositories-on-a-centos-6-vps)



