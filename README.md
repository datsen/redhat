![Alt text](/media/Red_Hat_logo_1.svg.png)

-------------


# **大綱**
-  ## [RHCE認證](/licence/README.md)
-  ## [RedHat 簡介 待補]()
-  ## [安裝Linux RedHat](/SetUp/README.md) 
-  ## [實作問題 待補](/manual/README.md)
#  RCHE系列
  - ## RCHE123
    - [CHAP1](/SetUp/install/configsetting/README.md)
      - Authority
      - touch
      - \>>
      - grep
      - less \ more

    - CHAP2
      - storage
      - dev
      - mount
      - pipeline
    - CHAP3
  - ## RCHE124
    - mkdir
    - CHAP2
    - CHAP3
  - ## RCHE294
    - playbook
    - ansible
    - process
    - package
    - 
# **模擬試題**
資料補充中


> **這邊是自己在練習以及實作的過程中所做的紀錄，歡迎大家一起討論**