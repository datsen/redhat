# yum容器設定

>1. **撰寫yum套件容器(repository)設定黨.repo，以建立yum安裝來源。**

    [root@demo]# cat /etc/yum.repos.d/example.repo

    [demo]   
    name=demo
    baseurl=youturl
    enabled=1
    gpgcheck=0
    gpgkey=yourkey
    # [base]:容器名稱，中括號必須存在，名稱可以自訂但不可重複。
    # name:說明這容器的意義。
    # mirrotlist=列出這個容器可以使用的印社站台，如果不想使用，可以註解。
    # baseurl=：指定固定容器的實際網址。
    # enable=1：啟動容器。如果不想啟動可以使用 enable=0。
    # gpgcheck=1：指定是否需要查閱 RPM 檔案內的數位簽章。
    # gpgkey=：數位簽章的公鑰檔所在位置。
>2. 列出yum使用的所有容器

    [root@demo ~]# yum repolist all
    repo id         repo name                                        status
    rhel            Red Hat Enterprise Linux 6.0                     enabled: 6,019
    rhel-beta       Red Hat Enterprise Linux 6.0 Beta                enabled: 1,042

>3. 容器資料刪除

    [root@demo ~]# yum clean [packages|headers|all] 
    選項與參數：
    packages：將已下載的軟體檔案刪除
    headers ：將下載的軟體檔頭刪除
    all     ：將所有容器資料都刪除

    #刪除已下載過的所有容器的相關資料 (含軟體本身與清單)
    [root@demo ~]# yum clean all
