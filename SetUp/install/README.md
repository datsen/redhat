# VMWARE、VirtualBox

>1. 安裝來源
   [REDHAT官網](https://developers.redhat.com/products/rhel/download)，如果沒在官網註冊過帳號，會要求註冊一個帳號。為什麼需要新增帳號[[1 待補](/SetUp/register/README.md)]
>2. VMWare
>   ![Alt](/SetUp/install/media/selectinstallation.png)
> 選擇要安裝的版本
> ![Alt](/SetUp/install/media/rhelx86.png)
> VM的帳號
> ![Alt](/SetUp/install/media/account.png)
> 選擇Cutomize Settings
> ![Alt](/SetUp/install/media/customize.png)
> 基本Memory 使用4G、Hard Disk使用20G、Network Adpater 使用 Auto 
> ![Alt](/SetUp/install/media/setting.png)
> ![Alt](/SetUp/install/media/start.png)
> 這邊為GUI設定，安裝完畢可以再去[config]()裡修改
> ![Alt](/SetUp/install/media/installationsumarry.png)
> 
> ![Alt](/SetUp/install/media/starting.png)

> ![Alt](/SetUp/install/media/redhatselected.png)
> ![Alt](/SetUp/install/media/login.png)
