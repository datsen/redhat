# CHAPER14 Installing and Updating Software Packages

> 相關指令
>1. Register 
    將目前系統註冊到Red Hat 
>2. Subscribe
>3. Enable repositories

> ##  Command Line
> - subscription-manger register --username=yourname --password=yourpassword
> - subscription-manger list --avaiable | less
> - subscription-manger  attatch --auto
> - subscription-manger  attach --pool=poolID
> - subscription-manger list --consumed
> -subscription-manger unregister